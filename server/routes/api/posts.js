const express = require('express');
const mongodbs = require('mongodb');
const mongodbClient = require('mongodb').MongoClient;
const router = express.Router();
//GET POSTS
router.get('/', async (req, res) => {
    const posts = await loadPostsCollections();
    res.send(await posts.find({}).toArray());
})
// ADD POSTS
router.post('/', async (req, res) => {
    const posts = await loadPostsCollections();
    await posts.insertOne({
        text: req.body.text,
        createdAt: new Date()
    })
    res.status(201).send(); 
})

// DELETE POSTS
router.delete('/:id', async (req, res) => {
    const posts = await loadPostsCollections();
    await posts.deleteOne({_id: new mongodbs.ObjectId(req.params.id)});
    res.status(200).send();
})

router.post('/signup', (req,res) =>{
    console.log(req.body);
    var insObj = {
        name: req.body.name,
        username: req.body.username,
        password: req.body.password
    }
    res.json({"msg":"form submitted"})
})

async function loadPostsCollections() {
    const client = await mongodbClient.connect('mongodb://localhost:27017/blogpostdb', { useUnifiedTopology: true });
    return client.db('blogpostdb').collection('posts')
}
async function userTableCollections() {
    const client = await mongodbClient.connect('mongodb://localhost:27017/blogpostdb', { useUnifiedTopology: true });
    return client.db('blogpostdb').collection('users')
}
module.exports = router;
